# OneWire Simulator

A Device Simulator to simulate OneWire devices such as 18B20 thermometer.

The system should be capable to simulate:
- 4x OneWire Buses and
- 4x 18B20 OneWire temperature sensors on each bus.

## system hardware

The project uses OLIMEX STM32E407 PCB.

On the UEXT I2C interface an DS2484 I2C to OneWire bridge is connected to interface the OneWire input.

## Development

The project is developed using ARM-GCC C language using:
- Visual Studio Code
- SEGGER JLink,
- SEGGER Ozone,
- SEGGER RTT View
- Saleae Logic

## references

- https://www.analog.com/en/products/ds18b20.html
- https://www.analog.com/en/products/ds2484.html
- https://www.olimex.com/Products/ARM/ST/STM32-E407/open-source-hardware
- https://developer.arm.com/downloads/-/arm-gnu-toolchain-downloads
- https://code.visualstudio.com/
- https://www.segger.com/products/debug-probes/j-link/
- https://www.saleae.com/


