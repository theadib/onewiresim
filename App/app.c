

#include <string.h>

#include "stm32f4xx_hal.h"
#include "main.h"

#include "stm32f4xx_ll_tim.h"
#include "stm32f4xx_ll_gpio.h"

#include "app.h"

#include "SEGGER_RTT.h"
#include "ds18b20.h"

extern I2C_HandleTypeDef hi2c1;
extern TIM_HandleTypeDef htim2;

/*
    BUS RESET:
    bus must be the time t_rst low then 
    after high period of t_pdih a low presence pulse is generated of duration of t_pdlow
    t_rst 480..960usec
    t_pdih 15..60usec
    t_pdlow 60..240usec
*/
// timer runs @21MHz
#define ONEWIRE_NANO_TO_TICKS(nano) ((nano * 21UL) / 1000UL)
#define ONEWIRE_TICKS_TO_NANO(ticks) ((ticks * 1000UL) / 21UL)

typedef enum {
    ONEWIRE_STATE_NONE = 0,
    ONEWIRE_STATE_RESET_START,  // wait for end reset pulse
    ONEWIRE_STATE_RESET_STOP,  // wait for alarm to set the presence pulse
    ONEWIRE_STATE_PRESENCE, // wait for releasing the presence pulse
    ONEWIRE_STATE_ROM_COMMAND, // master is clocking ROM command
    ONEWIRE_STATE_FUNCTION_COMMAND, // master is clocking function command
    ONEWIRE_STATE_DATA_OUT, // master is reading data
    ONEWIRE_STATE_SEARCH1, // master is reading 1st bit of triplet
    ONEWIRE_STATE_SEARCH2, // master is reading 2nd bit of triplet
    ONEWIRE_STATE_SEARCH3, // master is writing 3rd bit of triplet
    ONEWIRE_STATE_SEARCH_COMPARE, // comparing last bit against masters
    ONEWIRE_STATE_MATCH_ROM, // compare the ROM code against master data
} onewire_state_t;

typedef struct {
    onewire_state_t state;
    uint32_t state2;    // substate of state, used for counting bits
    uint8_t data[9];      // data transferred, capacity for 8byte plus crc

    uint32_t last_capture;  // time of last transition, depending on current state
} onewire_ctx_t;

static volatile onewire_ctx_t onewire_ctx;


/*
    the timer is used to time events and also generate interrupts after x*nanoseconds
    the timer resolution must be better than 0.25usec (used for 1wire overdrive mode)
    the used timer TIM2 runs @18MHz and is 32bit wide and has 4 capture/compare registers
*/
static void delay_nano(uint32_t nano)
{
    const uint32_t start = LL_TIM_GetCounter(htim2.Instance);
    const uint32_t diff = ONEWIRE_NANO_TO_TICKS(nano);
    while(LL_TIM_GetCounter(htim2.Instance) < (start +diff)) {

    }
}

// retireve the 32bit timestamp
static uint32_t time_nano(void)
{
    return LL_TIM_GetCounter(htim2.Instance);
}

static void onewire_line_low(int channel)
{
    LL_GPIO_ResetOutputPin(OWIRE_GPIO_Port, OWIRE_Pin);
    LL_GPIO_SetPinMode(OWIRE_GPIO_Port, LL_GPIO_PIN_10, LL_GPIO_MODE_OUTPUT);
    LL_GPIO_SetPinOutputType(OWIRE_GPIO_Port, LL_GPIO_PIN_10, LL_GPIO_OUTPUT_OPENDRAIN);
    LL_GPIO_SetPinSpeed(OWIRE_GPIO_Port, LL_GPIO_PIN_10, LL_GPIO_SPEED_FREQ_HIGH);
    LL_GPIO_SetPinPull(OWIRE_GPIO_Port, LL_GPIO_PIN_10, LL_GPIO_PULL_UP);

}

static void onewire_line_high(int channel)
{
    LL_GPIO_SetOutputPin(OWIRE_GPIO_Port, OWIRE_Pin);
    LL_GPIO_SetPinMode(OWIRE_GPIO_Port, LL_GPIO_PIN_10, LL_GPIO_MODE_INPUT);
}

static void onewire_init(void)
{
    memset((void *)&onewire_ctx, 0, sizeof(onewire_ctx));
}

static void onewire_alarm(int channel); // forward declaration
static void onewire_set_alarm(int channel, uint32_t tick)
{
    uint32_t cnt = LL_TIM_GetCounter(htim2.Instance);
    // SEGGER_RTT_printf(0, "\nONEWIRE @%lu set alarm %lu -> %lu. ", HAL_GetTick(), cnt, tick);
    uint32_t diff = tick - cnt;
    LL_TIM_OC_SetCompareCH1(htim2.Instance, tick);
    if(diff > (uint32_t)INT32_MAX) {
        SEGGER_RTT_WriteString(0, "elapsed ... call now. ");
        onewire_alarm(channel);
    } else {
        LL_TIM_ClearFlag_CC1(htim2.Instance);   // reset potential pending flags
        LL_TIM_EnableIT_CC1(htim2.Instance);    // enablle the interrupt
    }
}

static void onewire_reset_alarm(int channel)
{
    LL_TIM_DisableIT_CC1(htim2.Instance);
}

static void onewire_alarm(int channel)
{
    onewire_reset_alarm(channel);

    // SEGGER_RTT_printf(0, "\nONEWIRE @%ld alarm captured %lu := %lu. state: %u. ", HAL_GetTick(), htim2.Instance->CNT, htim2.Instance->CCR1, onewire_ctx.state);
    switch(onewire_ctx.state) {
        case ONEWIRE_STATE_RESET_START:
        // reset pulse too long
        onewire_ctx.state = ONEWIRE_STATE_NONE;
        break;

        case ONEWIRE_STATE_RESET_STOP:
        // start the presence pulse
        onewire_ctx.state = ONEWIRE_STATE_PRESENCE;
        SEGGER_RTT_printf(0, "generate presence pulse. ");
        onewire_line_low(0);
        onewire_set_alarm(0, time_nano() + ONEWIRE_NANO_TO_TICKS(80000U));
        break;

        case ONEWIRE_STATE_PRESENCE:
        // stop the presence pulse
        SEGGER_RTT_printf(0, "end presence pulse. ");
        onewire_line_high(0);
        onewire_ctx.state = ONEWIRE_STATE_ROM_COMMAND;
        onewire_ctx.state2 = 8; // 8 bits to count
        onewire_ctx.data[0] = 0;
        break;

        case ONEWIRE_STATE_ROM_COMMAND:
        // latch data from line
        onewire_ctx.data[0] >>= 1;
        if(LL_GPIO_IsInputPinSet(OWIRE_GPIO_Port, OWIRE_Pin)) {
            onewire_ctx.data[0] |= 0x80;
            SEGGER_RTT_WriteString(0, "H");
        } else {
            SEGGER_RTT_WriteString(0, "L");
        }
        if(onewire_ctx.state2 == 0) {
            SEGGER_RTT_printf(0, "\n*** end ROM Command %X.", onewire_ctx.data[0]);
            if(onewire_ctx.data[0] == 0xCC) {
                // skip ROM command
                onewire_ctx.state = ONEWIRE_STATE_FUNCTION_COMMAND;
                onewire_ctx.state2 = 8;
                onewire_ctx.data[0] = 0;
            } else if(onewire_ctx.data[0] == 0x55) {
                // match ROM command
                onewire_ctx.state = ONEWIRE_STATE_MATCH_ROM;
                onewire_ctx.state2 = 64;   // 64bits
                SEGGER_RTT_printf(0, "\nONEWIRE MATCH ROM. ");
            } else if(onewire_ctx.data[0] == 0xF0) {
                // search bus
                onewire_ctx.state = ONEWIRE_STATE_SEARCH1;
                memcpy(onewire_ctx.data, "(123456", 7);   // copy 8bit Family code, 48bit ID into buffer
                onewire_ctx.data[7] = w1_calc_crc8(onewire_ctx.data, 7); // calculate CRC
                onewire_ctx.state2 = 64;   // 64bits
                SEGGER_RTT_printf(0, "\nONEWIRE SEARCH START. ");
            } else {
                onewire_ctx.state = ONEWIRE_STATE_NONE;
            }

        }
        break;

        case ONEWIRE_STATE_FUNCTION_COMMAND:
        // latch data from line
        onewire_ctx.data[0] >>= 1;
        if(LL_GPIO_IsInputPinSet(OWIRE_GPIO_Port, OWIRE_Pin)) {
            onewire_ctx.data[0] |= 0x80;
            SEGGER_RTT_WriteString(0, "H");
        } else {
            SEGGER_RTT_WriteString(0, "L");
        }
        if(onewire_ctx.state2 == 0) {
            SEGGER_RTT_printf(0, "\n*** end function Command %X.", onewire_ctx.data[0]);
            if(onewire_ctx.data[0] == 0x44) {
                // start conversion
                onewire_ctx.state = ONEWIRE_STATE_NONE;
            } else if(onewire_ctx.data[0] == 0xBE) {
                // read scratchpad
                onewire_ctx.state = ONEWIRE_STATE_DATA_OUT;
                memcpy(onewire_ctx.data, "123456789", 9);
                onewire_ctx.data[8] = w1_calc_crc8(onewire_ctx.data, 8);
                onewire_ctx.state2 = 9*8;   // 9 bytes x8bits
                SEGGER_RTT_printf(0, "\nONEWIRE DATA OUT -> %X. ", onewire_ctx.data[0]);
            } else {
                onewire_ctx.state = ONEWIRE_STATE_NONE;
            }
        }
        break;

        case ONEWIRE_STATE_DATA_OUT:
        case ONEWIRE_STATE_SEARCH1:
        case ONEWIRE_STATE_SEARCH2:
        case ONEWIRE_STATE_SEARCH3:
        onewire_line_high(0);
        break;

        case ONEWIRE_STATE_SEARCH_COMPARE:
        // latch bit from line and compare
        if(LL_GPIO_IsInputPinSet(OWIRE_GPIO_Port, OWIRE_Pin)) {
            if(onewire_ctx.data[0] & 0x01) {
                SEGGER_RTT_WriteString(0, " match. ");

            } else {
                SEGGER_RTT_WriteString(0, " MISmatch. ");
                onewire_ctx.state = ONEWIRE_STATE_NONE;
            }
        } else {
            if((onewire_ctx.data[0] & 0x01) == 0) {
                SEGGER_RTT_WriteString(0, " match. ");
            } else {
                SEGGER_RTT_WriteString(0, " MISmatch. ");
                onewire_ctx.state = ONEWIRE_STATE_NONE;
            }
        }
        // if match then continue next bit
        if(onewire_ctx.state != ONEWIRE_STATE_NONE) {
            SEGGER_RTT_WriteString(0, " next bit. ");
            if(onewire_ctx.state2 > 1) {
                onewire_ctx.state2--;
                if(onewire_ctx.state2 %8 == 0) {
                    // move bytes in buffer
                    for(int i = 0; i < 64/8 -1; i++) {
                        onewire_ctx.data[i] = onewire_ctx.data[i+1];
                    }
                } else {
                    onewire_ctx.data[0] >>= 1;
                }
                onewire_ctx.state = ONEWIRE_STATE_SEARCH1;
            } else {
                onewire_ctx.state = ONEWIRE_STATE_NONE;
            }
        }
        break;

        case ONEWIRE_STATE_MATCH_ROM:
        if(onewire_ctx.state2) {
            onewire_ctx.state2--;
            unsigned int byte_idx = 7 - (onewire_ctx.state2 / 8); // where onewire_ctx.state2 is [0..63]
            // latch in bit
            if(LL_GPIO_IsInputPinSet(OWIRE_GPIO_Port, OWIRE_Pin)) {
                onewire_ctx.data[byte_idx] = (onewire_ctx.data[byte_idx] >> 1) | 0x80;
            } else {
                onewire_ctx.data[byte_idx] = onewire_ctx.data[byte_idx] >> 1;
            }

            // compare ROM code
            if(onewire_ctx.state2 == 0) {
                if((onewire_ctx.data[7] == w1_calc_crc8(onewire_ctx.data, 7)) && (memcmp(onewire_ctx.data, "(1234567", 7) == 0)) {
                    // ROM code matches; now detect FUNCTION COMMAND
                    onewire_ctx.state = ONEWIRE_STATE_FUNCTION_COMMAND;
                    onewire_ctx.state2 = 8;
                    onewire_ctx.data[0] = 0;
                    SEGGER_RTT_WriteString(0, "\nMATCH ROM: match. ");
                } else {

                    onewire_ctx.state = ONEWIRE_STATE_NONE;
                    SEGGER_RTT_WriteString(0, "\nMATCH ROM: MISmatch. ");
                }

            }


        } else {
            onewire_ctx.state = ONEWIRE_STATE_NONE;
        }
        break;


        case ONEWIRE_STATE_NONE:
        default:
    }
}

// transition of channel @param index, to @param level
static void onewire_transition(int index, int level)
{
    // SEGGER_RTT_printf(0, " @%lu:%lu transition ->%d *%u* ", HAL_GetTick(), time_nano(), level, onewire_ctx.state);
    static uint32_t last_now = 0;
    const uint32_t now = time_nano();

    // detect reset pulse in any case
    if(level && ((now-last_now) > ONEWIRE_NANO_TO_TICKS(480000U))) {
        onewire_ctx.state = ONEWIRE_STATE_RESET_STOP;
        onewire_ctx.last_capture = now;
        onewire_set_alarm(0, now + ONEWIRE_NANO_TO_TICKS(20000UL));   // activate presence in 20usec
    } else {
            
        switch(onewire_ctx.state) {
            case ONEWIRE_STATE_NONE:
            if(level == 0) {
                onewire_ctx.last_capture = now;
                onewire_ctx.state = ONEWIRE_STATE_RESET_START;
                onewire_set_alarm(0, now + ONEWIRE_NANO_TO_TICKS(1000000UL)); // timeout in 1msec
                // SEGGER_RTT_printf(0, "\nONEWIRE @%ld reset pulse started. ", now);
                SEGGER_RTT_WriteString(0, "\nONEWIRE reset pulse started. ");
                
            } else {
                onewire_reset_alarm(0);
            }
            break;
            case ONEWIRE_STATE_RESET_START:
            if(level) {
                const uint32_t diff = now - onewire_ctx.last_capture;
                if(diff > ONEWIRE_NANO_TO_TICKS(480000U) ) {
                    // SEGGER_RTT_printf(0, "\nONEWIRE @%ld reset pulse detected: %lu: diff %lu. ", HAL_GetTick(), now, ONEWIRE_TICKS_TO_NANO(diff));
                    onewire_ctx.state = ONEWIRE_STATE_RESET_STOP;
                    onewire_ctx.last_capture = now;
                    onewire_set_alarm(0, now + ONEWIRE_NANO_TO_TICKS(20000UL));   // activate presence in 20usec
                } else {
                    // SEGGER_RTT_printf(0, "\nONEWIRE @%ld reset pulse too short: diff %d. ", now, ONEWIRE_TICKS_TO_NANO(diff));
                    SEGGER_RTT_WriteString(0, "\nONEWIRE reset pulse too short: diff . ");
                    onewire_ctx.state = ONEWIRE_STATE_NONE;
                    onewire_reset_alarm(0);
                }
            } else {
                onewire_ctx.state = ONEWIRE_STATE_NONE;
            }
            break;

            case ONEWIRE_STATE_RESET_STOP:
            if(level == 0) {
                const uint32_t diff = now - onewire_ctx.last_capture;
                if(diff > ONEWIRE_NANO_TO_TICKS(1000000)) {
                    SEGGER_RTT_printf(0, "\nONEWIRE @%lu no presence in time diff: %lu. ", now, ONEWIRE_TICKS_TO_NANO(diff));
                    onewire_ctx.state = ONEWIRE_STATE_NONE;
                    onewire_reset_alarm(0);
                }
            }
            break;
            
            case ONEWIRE_STATE_PRESENCE:
            if(level) {
                uint32_t diff = now - onewire_ctx.last_capture;
            
                SEGGER_RTT_printf(0, "\nONEWIRE @%ld presence pulse: diff %d. ", now, ONEWIRE_TICKS_TO_NANO(diff));
            }
            break;

            case ONEWIRE_STATE_ROM_COMMAND:
            case ONEWIRE_STATE_FUNCTION_COMMAND:
            if(level == 0) {
                if(onewire_ctx.state2) {
                    // bit starts at low level edge , sample after 30usec
                    onewire_set_alarm(0, now + ONEWIRE_NANO_TO_TICKS(30000UL));   // latch data in 30usec
                    onewire_ctx.state2 -= 1;
                    SEGGER_RTT_WriteString(0, "latch. ");
                } else {
                    // too much bits
                    onewire_ctx.state = ONEWIRE_STATE_NONE;
                    onewire_reset_alarm(0);
                    SEGGER_RTT_printf(0, "\nONEWIRE @%ld reset TX COMMAND -> %X. ", now, onewire_ctx.data[0]);
                }
            }
            break;

            case ONEWIRE_STATE_DATA_OUT:
            if(level == 0) {
                if(onewire_ctx.state2) {
                    // shift out on low edge, keep 30usec low
                    if((onewire_ctx.data[0] & 0x01) == 0) {
                        onewire_line_low(0);
                        onewire_set_alarm(0, now + ONEWIRE_NANO_TO_TICKS(30000UL));   // keep 30usec
                    }

                    // prepare next
                    onewire_ctx.state2--;
                    if((onewire_ctx.state2 % 8) == 0) {
                        // move bytes in buffer
                        for(unsigned int i = 0; i < 8; i++) {
                            onewire_ctx.data[i] = onewire_ctx.data[i+1];
                        }
                        SEGGER_RTT_printf(0, "\nONEWIRE @%ld shift out -> %X. ", now, onewire_ctx.data[0]);
                    } else {
                        onewire_ctx.data[0] >>= 1;
                    }
                } else {
                    // too much bits
                    onewire_ctx.state = ONEWIRE_STATE_NONE;
                    onewire_reset_alarm(0);
                }
            }
            break;

            case ONEWIRE_STATE_SEARCH1:
            if(level == 0) {
                SEGGER_RTT_WriteString(0, " 1st-bit. ");
                if(onewire_ctx.state2) {
                    // shift out on low edge, keep 30usec low
                    if((onewire_ctx.data[0] & 0x01) == 0) {
                        onewire_line_low(0);
                        onewire_set_alarm(0, now + ONEWIRE_NANO_TO_TICKS(30000UL));   // keep 30usec
                    }
                    onewire_ctx.state = ONEWIRE_STATE_SEARCH2;
                } else {
                    // too much bits
                    onewire_ctx.state = ONEWIRE_STATE_NONE;
                    onewire_reset_alarm(0);
                }
            }
            break;

            case ONEWIRE_STATE_SEARCH2:
            if(level == 0) {
                SEGGER_RTT_WriteString(0, " 2nd-bit. ");
                // shift out negation on low edge, keep 30usec low
                if((onewire_ctx.data[0] & 0x01) != 0) {
                    onewire_line_low(0);
                    onewire_set_alarm(0, now + ONEWIRE_NANO_TO_TICKS(30000UL));   // keep 30usec
                }
                onewire_ctx.state = ONEWIRE_STATE_SEARCH3;
            }
            break;

            case ONEWIRE_STATE_SEARCH3:
            if(level == 0) {
                // bit starts at low level edge , sample after 30usec
                onewire_set_alarm(0, now + ONEWIRE_NANO_TO_TICKS(30000UL));   // latch data in 30usec
                onewire_ctx.state = ONEWIRE_STATE_SEARCH_COMPARE;
                SEGGER_RTT_WriteString(0, " 3rd-bit. ");
            }
            case ONEWIRE_STATE_SEARCH_COMPARE:
            break;

            case ONEWIRE_STATE_MATCH_ROM:
            if(level == 0) {
                if(onewire_ctx.state2) {
                    // bit starts at low level edge , sample after 30usec
                    onewire_set_alarm(0, now + ONEWIRE_NANO_TO_TICKS(30000UL));   // latch data in 30usec
                } else {
                    onewire_ctx.state = ONEWIRE_STATE_NONE;
                }
            }
            break;

            default:
            SEGGER_RTT_printf(0, "\nONEWIRE @%ld transition -> %d. ", now, level);
            onewire_reset_alarm(0);
            onewire_ctx.state = ONEWIRE_STATE_NONE;
        }
    }

    last_now = now;
    // SEGGER_RTT_printf(0, " *%u* ", onewire_ctx.state);
}

/**
    exti callback on high/low or low/high transition
    the interrupt is already handled by the HAL drivers
*/
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
    // channel 0 GPIOB:10
    if(GPIO_Pin == OWIRE_Pin) {
        if(LL_GPIO_IsInputPinSet(OWIRE_GPIO_Port, OWIRE_Pin)) {
            SEGGER_RTT_WriteString(0, "-1-");
        } else {
             SEGGER_RTT_WriteString(0, "-0-");
        }
        onewire_transition(0, LL_GPIO_IsInputPinSet(OWIRE_GPIO_Port, OWIRE_Pin));
    }
}

void HAL_TIM_OC_DelayElapsedCallback(TIM_HandleTypeDef *htim)
{
    if(htim == &htim2) {
        if(htim->Channel == HAL_TIM_ACTIVE_CHANNEL_1) {
            LL_TIM_DisableIT_CC1(htim2.Instance);
            onewire_alarm(0);
        }
    }

}


void app_init(void)
{
    SEGGER_RTT_WriteString(0, "\n\n\n--INIT--.\n");
    onewire_init();
    ds18b20_init(&hi2c1);
    HAL_TIM_Base_Start(&htim2);
    HAL_TIM_OC_Start_IT(&htim2, HAL_TIM_ACTIVE_CHANNEL_1);
    LL_TIM_DisableIT_CC1(htim2.Instance);
}

void app_idle(void)
{
    HAL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin);
    HAL_GPIO_WritePin(DEBUG1_GPIO_Port, DEBUG1_Pin, 1);
    delay_nano(1000000UL);
    HAL_GPIO_WritePin(DEBUG0_GPIO_Port, DEBUG1_Pin, 0);

    HAL_GPIO_WritePin(DEBUG0_GPIO_Port, DEBUG0_Pin, 1);
    ds18b20_startconversion();
    HAL_Delay(100);
    
    int temperature = 0;
    int err = ds18b20_read(&temperature);
    if(err) {
        SEGGER_RTT_WriteString(0, "\n### failure ###. ");
    } else {
        SEGGER_RTT_printf(0, "\ntemperature: %d. ", temperature);
    }
    HAL_GPIO_WritePin(DEBUG0_GPIO_Port, DEBUG0_Pin, 0);
    
    HAL_Delay(400);

    uint64_t devices[10];
    int found = w1_device_search(devices, 10);

    if(found > 0) {
        for(int i = 0; i < found; i++) {
            SEGGER_RTT_printf(0, "\r\n +++ W1 search found: %X:%X. ", 
                    (unsigned int)(devices[i]>>32), (unsigned int)devices[i]);
            err = ds18b20_read_address(&temperature, devices[i]);
        }
    } else {
        SEGGER_RTT_printf(0, "\r\n +++ W1 search found ### nothing ###. ");
    }


}