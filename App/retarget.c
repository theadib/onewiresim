#include <stdio.h>

void retarget_init()
{
  // Initialize UART
}

int _write (int fd, char *ptr, int len)
{
  /* Write "len" of char from "ptr" to file id "fd"
   * Return number of char written.
   * Need implementing with UART here. */
  return len;
}

int _read (int fd, char *ptr, int len)
{
  /* Read "len" of char to "ptr" from file id "fd"
   * Return number of char read.
   * Need implementing with UART here. */
  return len;
}

int _close(int)
{
  return 0;
 }

off_t _lseek (int filedes, off_t offset, int whence)
{
  return 0;
}


void _ttywrch(int ch) {
  /* Write one char "ch" to the default console
   * Need implementing with UART here. */
}

/* SystemInit will be called before main */
/*
void SystemInit()
{
    retarget_init();
}
*/
